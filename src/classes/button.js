export default class Button {
    constructor(title, className, id) {
        this.title = title;
        this.className = className;
        this.id = id;
    }

    render() {
        let btn = document.createElement('button');
        btn.innerText = this.title;
        btn.setAttribute("class", this.className);
        btn.id = this.id;
        return btn;
    }
}