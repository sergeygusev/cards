import Button from "../classes/button";
import Select from "../classes/select";
import Form from "../classes/form";
import Input from "../classes/input";
import Modal from "../classes/modal";
import {editCard} from "./editCard";

export function editBtnFunc(token, id, obj) {
    let content = document.querySelector('.content');
    let modal = new Modal('Редактирование', []);

    content.insertAdjacentHTML('afterbegin', modal.render());

    let editVisitContent = document.querySelector('.popup__text');
    let editForm = new Form('editVisitForm', []);

    editVisitContent.append(editForm.render());

    let currentForm = document.querySelector('.form');

    for (let dataKey in obj) {
        switch (dataKey) {

            case 'doctor':
                currentForm.append(new Input('hidden', 'input', dataKey, obj[dataKey], obj[dataKey]).render());
                break;

            case 'urgency':
                currentForm.append(
                    new Select([
                        'Обычная',
                        'Приоритетная',
                        'Неотложная'
                    ], 'select', 'urgency', obj[dataKey], 'Выберите срочность').render(),
                );
                break;

            default:
                currentForm.append(new Input('text', 'input', dataKey, obj[dataKey], obj[dataKey]).render());
                break;
        }
    }

    let editVisitBtn = new Button('Редактировать визит', 'btn', 'edit-visit-button');
    editVisitContent.append(editVisitBtn.render());

    let editBtn = document.getElementById('edit-visit-button');

    editBtn.addEventListener('click', () => {
        let form = document.forms.editVisitForm;
        editCard(token, form, id, modal);
    });

    let closePopupBtn = document.querySelector('.close');
    closePopupBtn.addEventListener('click', () => {
        modal.close();
    });
}