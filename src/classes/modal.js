export default class Modal {
    constructor(title, items = []) {
        this.title = title;
        this.items = items;
    }

    close() {
        let modalWrapper = document.querySelector('.popup-wrap');
        modalWrapper.classList.remove('popup-wrap_active');
    }

    render() {
        let popupWindow = document.getElementsByClassName("popup-wrap");
        [...popupWindow].forEach(elem => {
            elem.remove();
        });

        let str = '';

        this.items.map(elem => {
            str += elem.outerHTML;
        });

        return `<div class="popup-wrap popup-wrap_active"><div class="popup"><h3 class="popup__caption">${this.title}</h3><div class="close"></div><div class="popup__text">${str}</div></div></div>`;
    }
}