export function validate(data) {
    let elems = data.elements;
    let arr = [];

    [...elems].forEach(el => {
        if (el.type !== 'hidden') {
            if (el.value == 0 || el.value === "") {
                arr.push(el.value)
            }
        }
    });

    return arr.length;
}