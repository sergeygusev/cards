import { deleteBtnFunc } from "./deleteBtnFunc";
import { editBtnFunc } from "./editBtnFunc";
import { drag } from "./drag";
import { findCard } from "./findCard";

export function renderCard(obj, data, id) {
    let cardsWrap = document.querySelector('.cards-wrap');
    let wrapCard
    let result = '';
    wrapCard = document.createElement("div");
    wrapCard.classList.add("card");

    for (let dataKey in obj) {
        if (dataKey === 'urgency') {
            if (obj[dataKey] === 'обычная') {
                wrapCard.setAttribute('data-urgency', '0')
            } else if (obj[dataKey] === 'приоритетная') {
                wrapCard.setAttribute('data-urgency', '1')
            } else if (obj[dataKey] === 'неотложная') {
                wrapCard.setAttribute('data-urgency', '2')
            }
        } else if (dataKey === 'date') {
            wrapCard.setAttribute('data-visit-date', `${obj[dataKey]}`)
        }
        result += `<li data-${dataKey}="${obj[dataKey]}" class="li-item">${obj[dataKey]}</li>`;
    }

    wrapCard.insertAdjacentHTML('afterbegin', `<ul id="${id}">${result}</ul>`)
    cardsWrap.append(wrapCard)

    wrapCard.addEventListener('mousedown', (e) => {
        if (e.target.tagName === "DIV") {
            drag(e, e.target)
        } else if (e.target.tagName === "LI") {
            drag(e, findCard(e.target))
        }
    })

    //кнопка редактировать
    let editBtn = document.createElement('button');
    editBtn.innerHTML = 'Edit';
    editBtn.classList.add('edit-btn');
    wrapCard.append(editBtn);
    editBtn.addEventListener('click', () => {
        editBtnFunc(data.token, id, obj);
    })

    // кнопка удалить
    let deleteCardBtn = document.createElement('button')
    deleteCardBtn.classList.add('delete-card-btn')
    wrapCard.prepend(deleteCardBtn)

    deleteCardBtn.addEventListener('click', () =>{
        deleteBtnFunc(data.token, id, deleteCardBtn)
    })
}