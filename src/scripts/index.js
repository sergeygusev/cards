"use strict"

let filtersWrap = document.querySelector('.filters-wrap')
let filterUrgency = document.querySelector('.filter-urgency');
let filterDate = document.querySelector('.filter-date');
let filterSearch = document.querySelector('.search');

import Button from "../classes/button";
import Select from "../classes/select";
import Input from "../classes/input";
import Form from "../classes/form";
import Modal from "../classes/modal";
import axios from "axios";

import { renderCard } from "../scripts/renderCard";
import { filterUrgencyFunc } from "../scripts/filterUrgencyFunc";
import { filterDateFunc } from "../scripts/filterDateFunc";
import { filterSearchFunc } from "../scripts/filterSearchFunc";
import { getCards } from "../scripts/getCards";
import { createCard } from "../scripts/createCard";
import { validate } from "../scripts/validate";

let login = document.querySelector('.register');

let loginBtn = new Button('Вход', 'btn', 'login-btn');

login.append(loginBtn.render());

let loginButton = document.getElementById('login-btn');

loginButton.addEventListener('click', () => {
    let signupLogin = new Input('text', 'input', 'login', 'E-mail');
    let signupPassword = new Input('password', 'input', 'password', 'Password');
    let signupBtn = new Button('Войти', 'btn', 'signup-btn');
    let content = document.querySelector('.content');

    let modal = new Modal('Вход', [
        signupLogin.render(),
        signupPassword.render(),
        signupBtn.render()
    ]);

    content.insertAdjacentHTML('afterbegin', modal.render());

    let closePopupBtn = document.querySelector('.close');
    closePopupBtn.addEventListener('click', () => {
        modal.close();
    });

    let sendSignup = document.getElementById("signup-btn");
    sendSignup.addEventListener('click', () => {
        let email = document.getElementById('login');
        let password = document.getElementById('password');

        axios.post('http://cards.danit.com.ua/login', {
            email: email.value,
            password: password.value
        })
            .then(function (response) {
                if (response.data.status === "Success") {
                    modal.close();

                    let noItemsText = document.querySelector('.no-items-text');
                    let allCards = getCards(response.data.token);

                    allCards
                      .then(cards => {
                        if (cards.data.length === 0) {
                          noItemsText.classList.remove('display-none');
                        } else {
                          filtersWrap.classList.remove('display-none');

                          cards.data.forEach(el => {
                            renderCard(el.obj, response.data, el.id)
                          })
                        }
                      })

                    login.innerHTML = '';

                    filterUrgency.addEventListener('change', () => {
                      filterUrgencyFunc(filterUrgency.value)
                    })

                    filterDate.addEventListener('change', () => {
                      filterDateFunc(filterDate.value)
                    })

                    filterSearch.addEventListener('keyup', () => {
                      filterSearchFunc(filterSearch.value)
                    })

                    let createVisitBtn = new Button('Создать визит', 'btn', 'create-visit-btn');

                    createVisitBtn.render();
                    login.append(createVisitBtn.render());


                    let createVisit = document.getElementById('create-visit-btn');
                    createVisit.addEventListener('click', () =>{


                        let selectDoctor = new Select([
                            'Кардиолог',
                            'Стоматолог',
                            'Терапевт'
                        ], 'select', 'select-doctor', 0, 'Выберите доктора');

                        let modal = new Modal('Создать визит', [
                            selectDoctor.render(),
                        ]);


                        content.insertAdjacentHTML('afterbegin', modal.render());

                        let closePopupBtn = document.querySelector('.close');
                        closePopupBtn.addEventListener('click', () => {
                          modal.close();
                        });

                        let doctor = selectDoctor.change();

                        doctor.addEventListener('change', () => {
                            let createVisitContent = document.querySelector('.popup__text');

                            while (createVisitContent.children.length > 1) {
                                createVisitContent.removeChild(createVisitContent.lastChild);
                            }

                            switch (doctor.value) {

                                case "кардиолог":
                                    let form = new Form('createVisitForm', [
                                        new Input('hidden', 'input', 'doctor', '', doctor.value).render(),
                                        new Input('text', 'input', 'target', 'Цель визита').render(),
                                        new Input('text', 'input', 'desc', 'Краткое описание визита').render(),
                                        new Input('date', 'input', 'date', 'Желаемая дата визита').render(),
                                        new Select([
                                            'Обычная',
                                            'Приоритетная',
                                            'Неотложная'
                                        ], 'select', 'urgency', 0, 'Выберите срочность').render(),
                                        new Input('text', 'input', 'press', 'Обычное давление').render(),
                                        new Input('text', 'input', 'mass', 'Индекс массы тела').render(),
                                        new Input('text', 'input', 'diseases', 'Перенесенные заболевания сердечно-сосудистой системы').render(),
                                        new Input('text', 'input', 'age', 'Возраст').render(),
                                        new Input('text', 'input', 'fio', 'Ф.И.О.').render(),
                                    ]);

                                    createVisitContent.append(form.render());

                                    let createVisitBtn = new Button('Создать визит', 'btn', 'create-visit-button');
                                    createVisitContent.append(createVisitBtn.render());

                                    break;

                                case "стоматолог":

                                    let formDant = new Form('createVisitForm', [
                                        new Input('hidden', 'input', 'doctor', '', doctor.value).render(),
                                        new Input('text', 'input', 'target', 'Цель визита').render(),
                                        new Input('text', 'input', 'desc', 'Краткое описание визита').render(),
                                        new Input('date', 'input', 'date', 'Желаемая дата визита').render(),
                                        new Select([
                                            'Обычная',
                                            'Приоритетная',
                                            'Неотложная'
                                        ], 'select', 'urgency', 0, 'Выберите срочность').render(),
                                        new Input('date', 'input', 'last', 'Дата последнего посещения').render(),
                                        new Input('text', 'input', 'fio', 'Ф.И.О.').render()
                                    ]);

                                    createVisitContent.append(formDant.render());

                                    let createVisitDantBtn = new Button('Создать визит', 'btn', 'create-visit-button');
                                    createVisitContent.append(createVisitDantBtn.render());

                                    break;

                                case "терапевт":

                                    let formTerapeut = new Form('createVisitForm', [
                                        new Input('hidden', 'input', 'doctor', '', doctor.value).render(),
                                        new Input('text', 'input', 'target', 'Цель визита').render(),
                                        new Input('text', 'input', 'desc', 'Краткое описание визита').render(),
                                        new Input('date', 'input', 'date', 'Желаемая дата визита').render(),
                                        new Select([
                                            'Обычная',
                                            'Приоритетная',
                                            'Неотложная'
                                        ], 'select', 'urgency', 0, 'Выберите срочность').render(),
                                        new Input('text', 'input', 'age', 'Возраст').render(),
                                        new Input('text', 'input', 'fio', 'Ф.И.О.').render()
                                    ]);

                                    createVisitContent.append(formTerapeut.render());

                                    let createVisitTerapeutBtn = new Button('Создать визит', 'btn', 'create-visit-button');
                                    createVisitContent.append(createVisitTerapeutBtn.render());

                                    break;
                            }


                            let createVisitButton = document.getElementById("create-visit-button");

                            createVisitButton.addEventListener('click', () => {

                                let form = document.forms.createVisitForm;
                                let valid = validate(form);

                                if (valid == 0) {
                                    let createResponse = createCard(response.data.token, form);

                                    createResponse
                                        .then(data => {
                                            modal.close();

                                            noItemsText.classList.add('display-none');
                                            filtersWrap.classList.remove('display-none');

                                            renderCard(data.data.obj, response.data, data.data.id)
                                        });
                                }


                            })
                        })
                    });
                } else {
                    console.log(response.data)
                }
            });
    })
});
