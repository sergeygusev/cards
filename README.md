# Step Project Cards

## What Been Used:

Bundler - [Parcel.js](https://en.parceljs.org).
Other dependencies you can find in package.json file.

## How to use:

For installing bundler parcel run command:

*npm install -g parcel-bundler*

For installing all dependencies run command :

*npm install*

For developing mode run command:

*npm run dev*

For bulding project run command:

*npm run build*

## Developers

### [Sergey Gusev](https://gitlab.com/sergeygusev)
Responsible for:
- Authorization;
- Create visit form;

### [Gleb Barsky](https://gitlab.com/glebars)
Responsible for:
- Editing visits
- Drag&Drop

### [Olya Kovaliuk](https://gitlab.com/olya.kovaliuk)
Responsible for:
- Project setup;
- Filters;
