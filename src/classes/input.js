export default class Input {
    constructor(type, className, id, placeholder, value = null) {
        this.type = type;
        this.className = className;
        this.id = id;
        this.placeholder = placeholder;
        this.value = value;
    }

    render() {
        let input = document.createElement('input');

        input.setAttribute("type", this.type);
        input.setAttribute("class", this.className);
        input.id = this.id;
        input.placeholder = this.placeholder;
        input.setAttribute("name", this.id);
        input.value = this.value;

        return input;
    }
}