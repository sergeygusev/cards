export default class Form {
    constructor(name, elements = []) {
        this.name = name;
        this.elements = elements;
    }

    render() {
        let form = document.createElement("form");
        form.setAttribute("name", this.name);
        form.setAttribute("class", "form");

        this.elements.forEach(el => {
          if (el.placeholder) {
            let label = document.createElement('label')
            label.classList.add('label');
            label.insertAdjacentHTML('afterbegin', `${el.placeholder}`)
            form.append(label)
          }
          form.append(el);
        });

        return form;
    }
}
