export default class Select {
    constructor(items = [], className, id, selected, defaultText) {
        this.items = items;
        this.className = className;
        this.id = id;
        this.selected = selected;
        this.defaultText = defaultText;
    }

    change() {
        return document.getElementById(this.id);
    }

    render() {
        let select = document.createElement('select');
        select.setAttribute('class', this.className);
        select.setAttribute('name', this.id);
        select.id = this.id;

        let options = this.items.map(elem => {
            let value = elem.toLowerCase();
            return `<option ${this.selected === value ? 'selected' : ''} value="${value}">${elem}</option>`;
        });

        options.unshift('<option value="0">- ' + this.defaultText + ' -</option>');
        select.innerHTML = options;

        return select;
    }
}