import axios from "axios/index";

export async function createCard(token, form) {
    let elems = form.elements;
    let obj = {};

    [...elems].forEach(el => {
        obj[el.name] = el.value;
    });

    return await axios.post('http://cards.danit.com.ua/cards', {
        obj
    }, {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    });
}