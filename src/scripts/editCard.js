import axios from "axios/index";
import {validate} from "./validate";

export async function editCard(token, form, id, modal) {
    let valid = validate(form);

    if (valid == 0) {
        let currentUl = document.getElementById(id);
        let dataUl = document.getElementById(id);
        let result = '';
        currentUl.innerHTML = '';

        let elems = form.elements;
        let obj = {};

        [...elems].forEach(el => {
            obj[el.name] = el.value;
        });

        return await axios.put(`http://cards.danit.com.ua/cards/${id}`, {
            obj
        }, {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        })
            .then(data => {
                if (data.statusText === "OK") {
                    modal.close();

                    for (let dataKey in obj) {
                        result += `<li data-${dataKey}="${obj[dataKey]}" class="li-item">${obj[dataKey]}</li>`;
                    }

                    dataUl.insertAdjacentHTML('afterbegin', result);
                }
            });
    }
}