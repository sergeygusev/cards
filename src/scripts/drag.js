import { getCoords } from "./getCoords";

export function drag(e, target) {

    let coords = getCoords(target);
    let shiftX = e.pageX - coords.left;
    let shiftY = e.pageY - coords.top;

    target.style.left = e.pageX - shiftX + 'px';
    target.style.top = e.pageY - shiftY + 'px';
    moveAt(target);
    target.style.position = 'absolute';
    document.body.appendChild(target);


    target.style.zIndex = '1000';

    function moveAt(e) {
        target.style.left = e.pageX - shiftX + 'px';
        target.style.top = e.pageY - shiftY + 'px';
    }

    document.onmousemove = function (e) {
        moveAt(e);
    }

    document.onmouseup = function () {
        document.onmousemove = null;
        target.onmouseup = null;
    }

    target.ondragstart = function () {
        return false;
    };
}