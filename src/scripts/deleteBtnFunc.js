import axios from "axios/index";

export async function deleteBtnFunc(token, cardID, item) {

    await axios.delete(`http://cards.danit.com.ua/cards/${cardID}`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    item.closest(".card").remove()
}
