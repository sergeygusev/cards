export function filterDateFunc(value) {
    let cards = document.querySelectorAll('.card');

    let today = new Date();
    let todayMonth = today.getMonth() + 1;
    let todayDay = today.getDate();

    let visitDate
    let dayVisit
    let monthVisit

    switch (value) {
        case 'done':
            cards.forEach((item, i) => {
                item.classList.add('display-none')
                visitDate = item.getAttribute('data-visit-date')
                dayVisit = parseInt(visitDate.slice(8, 10));
                monthVisit = parseInt(visitDate.slice(5, 7))

                if (todayMonth > monthVisit) {
                    item.classList.remove('display-none')
                } else if (todayMonth === monthVisit) {
                    if (todayDay > dayVisit) {
                        item.classList.remove('display-none')
                    }
                }

            });
            break;

        case 'open':
            cards.forEach((item, i) => {
                item.classList.add('display-none')
                visitDate = item.getAttribute('data-visit-date')
                dayVisit = parseInt(visitDate.slice(8, 10));
                monthVisit = parseInt(visitDate.slice(5, 7))

                if (todayMonth <= monthVisit) {
                    item.classList.remove('display-none')
                    if (todayDay <= dayVisit) {
                        item.classList.remove('display-none')
                    }
                }

            });
            break;

        case 'all':
            cards.forEach((item, i) => {
                item.classList.remove('display-none')
            });

            break;
    }
}