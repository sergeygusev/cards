export function filterUrgencyFunc(value) {
    let cards = document.querySelectorAll('.card');

    cards.forEach((item, i) => {
        item.classList.add('display-none')
    });

    switch (value) {
        case 'all':
            cards.forEach((item, i) => {
                item.classList.remove('display-none')
            });
            break;

        case 'regular':
            cards.forEach((item, i) => {
                if (item.getAttribute('data-urgency') === '0') {
                    item.classList.remove('display-none')
                }
            });
            break;

        case 'priority':
            cards.forEach((item, i) => {
                if (item.getAttribute('data-urgency') === '1') {
                    item.classList.remove('display-none')
                }
            });
            break;

        case 'emergency':
            cards.forEach((item, i) => {
                if (item.getAttribute('data-urgency') === '2') {
                    item.classList.remove('display-none')
                }
            });
            break;
    }
}