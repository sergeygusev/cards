import axios from "axios/index";

export async function getCards(token) {
    return await axios.get('http://cards.danit.com.ua/cards', {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    });
}